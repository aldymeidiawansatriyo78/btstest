USE [DBAPItes]
GO
/****** Object:  Table [dbo].[checklist_table]    Script Date: 18/03/2023 19:31:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[checklist_table](
	[checklistId] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[type] [varchar](50) NULL,
	[checklistItemId] [bigint] NOT NULL,
 CONSTRAINT [PK_checklist_table] PRIMARY KEY CLUSTERED 
(
	[checklistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
