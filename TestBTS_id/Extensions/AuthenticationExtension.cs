﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestBTS_id.DTOs;

namespace TestBTS_id.Extensions
{
    public static class AuthenticationExtension
    {
        public static IServiceCollection AddTokenAuthentication(
                this IServiceCollection services,
                IConfiguration config
            )
        {
            var jwtConfig = config.GetSection("JwtConfig");
            string secret = jwtConfig.GetSection("secret").Value;
            string issuer = jwtConfig.GetSection("issuer").Value;
            var key = Encoding.ASCII.GetBytes(secret);

            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.TokenValidationParameters = ConfigureTokenValidation(key: key, issuer: issuer);
                    x.Events = ConfigureJWTEvents();
                });

            return services;
        }

        static private TokenValidationParameters ConfigureTokenValidation(string issuer, byte[] key)
        {
            return new TokenValidationParameters
            {
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateAudience = false,
                ValidateLifetime = false,
                ValidIssuer = issuer,
                ValidAudience = issuer
            };
        }

        static private JwtBearerEvents ConfigureJWTEvents()
        {
            var errorAuthObject = new AuthResponseDTO
            {
                AuthVAResponse = new AuthVAResponse
                {
                    ResponseCode = "401",
                    ResponseMessage = "Token is not provided/not valid/expired"
                }
            };

            return new JwtBearerEvents
            {
                OnChallenge = async context =>
                {
                    // Call this to skip the default logic and avoid using the default response
                    context.HandleResponse();
                    var errResponse = JsonConvert.SerializeObject(errorAuthObject);
                    // Write to the response in any way you wish
                    context.Response.StatusCode = 401;
                    context.Response.Headers["content-type"] = "application/json";
                    await context.Response.WriteAsync(errResponse);
                }
            };
        }

    }
}
