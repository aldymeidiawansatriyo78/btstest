﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBTS_id.DTOs;
using TestBTS_id.Interfaces;

namespace TestBTS_id.Controllers
{
    [Authorize]
    [Route("[controller]/api")]
    [ApiController]
    public class BtsApi : ControllerBase
    { 
        private readonly IChecklistProvider _checklistProvider;
    
        public BtsApi (IChecklistProvider checklistProvider)
        {
            _checklistProvider = checklistProvider;
        }
        [HttpGet("checklist")]
        public async Task<IActionResult> Checklist()
        {
            var hasil = await _checklistProvider.GetChecklist();

            ResponseDTO response = new ResponseDTO();

            response.hasil = JsonConvert.SerializeObject(hasil);
            response.message = "success";
            response.status = 200;

            return StatusCode(response.status, response);
        }

        [HttpPost("checklist")]
        public async Task<IActionResult> CreateChecklist([FromBody] CheckNameDTO name)
        {
            var hasil = await _checklistProvider.InsertChecklist(name.name);

            ResponseDTO response = new ResponseDTO();

            if (string.IsNullOrEmpty(hasil))
            {
                response.hasil = "erorr saat insert";
                response.message = "success";
                response.status = 409;
            }
            else
            {
                response.hasil = hasil;
                response.message = "success";
                response.status = 200;
            }
            
            return StatusCode(response.status, response);
        }

        [HttpDelete("checklist/{checkId:int}")]
        public async Task<IActionResult> DeleteChecklist(int checkId)
        {
            int hasil = await _checklistProvider.RemoveChecklist(checkId);

            ResponseDTO response = new ResponseDTO();

            if (hasil == 0)
            {
                response.hasil = "erorr saat delete";
                response.message = "success";
                response.status = 409;
            }
            else
            {
                response.hasil = "berhasil menghapus data dengan checkId = "+ hasil;
                response.message = "success";
                response.status = 200;
            }

            return StatusCode(response.status, response);
        }

        [HttpGet("checklist/{checkId:int}/item")]
        public async Task<IActionResult> GetChecklistItem(int checkId)
        {
            var hasil = await _checklistProvider.GetChecklistItem(checkId);

            ResponseDTO response = new ResponseDTO();

            response.hasil = JsonConvert.SerializeObject(hasil);
            response.message = "success";
            response.status = 200;

            return StatusCode(response.status, response);
        }

        [HttpPost("checklist/{checkId:int}/item")]
        public async Task<IActionResult> CreateChecklistItem(int checkId, [FromBody] CheckNameDTO name)
        {
            var hasil = await _checklistProvider.AddChecklistItem(checkId, name.itemName);

            ResponseDTO response = new ResponseDTO();

            if (string.IsNullOrEmpty(hasil))
            {
                response.hasil = "erorr saat insert";
                response.message = "success";
                response.status = 409;
            }
            else
            {
                response.hasil = "berhasil menambahkan item = " + hasil;
                response.message = "success";
                response.status = 200;
            }

            return StatusCode(response.status, response);
        }

        [HttpGet("checklist/{checkId:int}/item/{checkItemId:int}")]
        public async Task<IActionResult> GetChecklistItemById(int checkId, int checkItemId)
        {
            var hasil = await _checklistProvider.GetChecklistItembyId(checkId, checkItemId);

            ResponseDTO response = new ResponseDTO();

            if (string.IsNullOrEmpty(hasil))
            {
                response.hasil = "data tidak ditemukan";
                response.message = "not found";
                response.status = 409;
            }
            else
            {
                response.hasil = hasil;
                response.message = "success";
                response.status = 200;
            }

            return StatusCode(response.status, response);
        }

      
        [HttpDelete("checklist/{checkId:int}/item/{checkItemId:int}")]
        public async Task<IActionResult> DeleteChecklistItembyId(int checkId, int checkItemId)
        {
            string hasil = await _checklistProvider.RemoveChecklistItemById(checkId, checkItemId);

            ResponseDTO response = new ResponseDTO();

            if (string.IsNullOrEmpty(hasil))
            {
                response.hasil = "erorr saat delete";
                response.message = "success";
                response.status = 409;
            }
            else
            {
                response.hasil = "berhasil menghapus data dengan checkId = " + hasil;
                response.message = "success";
                response.status = 200;
            }

            return StatusCode(response.status, response);
        }

        [HttpPut("checklist/{checkId:int}/item/rename/{checkItemId:int}")]
        public async Task<IActionResult> UpdateChecklistItemById(int checkId, int checkItemId, [FromBody] CheckNameDTO name)
        {
            var hasil = await _checklistProvider.UpdateChecklistItembyId(checkId, checkItemId, name.itemName);

            ResponseDTO response = new ResponseDTO();

            if (string.IsNullOrEmpty(hasil))
            {
                response.hasil = "data tidak ditemukan";
                response.message = "not found";
                response.status = 409;
            }
            else
            {
                response.hasil = "item berhasil diubah ke nama :" + hasil;
                response.message = "success";
                response.status = 200;
            }

            return StatusCode(response.status, response);
        }

    }
}
