﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBTS_id.DTOs;
using TestBTS_id.Interfaces;
using TestBTS_id.Model;

namespace TestBTS_id.Controllers
{
    [Route("BtsApi/api")]
    public class TokenController : ControllerBase
    {
        private readonly ILogger<TokenController> _logger;
        private readonly IJWTProvider _jwtProvider;

        public TokenController(ILogger<TokenController> logger, IJWTProvider jwtProvider)
        {
            _logger = logger;
            _jwtProvider = jwtProvider;

        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] LoginRequestDTO payload)
        {
            TokenResponseDTO token = null;
            string message;
            int status;

            try
            {
                if (await _jwtProvider.AuthenticateUser(payload))
                {
                    token = _jwtProvider.GenerateToken(payload.Username);
                    message = "Login success";
                    status = 200;
                }
                else
                {
                    message = "username or password do not match";
                    status = 400;
                }

            }
            catch (Exception err)
            {
                _logger.LogError(err.Message + Environment.NewLine + err.StackTrace);
                message = "Something went wrong, please try again a few minutes later";
                status = 500;
            }

            var responseDTO = new AuthResponseDTO
            {
                AuthVAResponse = new AuthVAResponse
                {
                    ResponseCode = Convert.ToString(status),
                    ResponseMessage = message,
                    DetailResponse = token
                }
            };

            return StatusCode(status, responseDTO);

        }

        [HttpPost("register")]
        public async Task<ActionResult<AuthResponseDTO>> Register([FromBody] RegisterRequestDTO payload)
        {
            int status;
            string message;

            try
            {
                if (payload.Password == payload.ConfirmationPassword)
                {
                    AuthorizeModel existingUser = await _jwtProvider.GetOneUser(payload.Username);

                    if (existingUser == null)
                    {
                        await _jwtProvider.RegisterUser(payload);
                        message = "Registration success";
                        status = 201;
                    }
                    else
                    {
                        message = "User already exists";
                        status = 400;
                    }
                }
                else
                {
                    message = "password and confirmationPassword do not match";
                    status = 400;
                }


            }
            catch (Exception err)
            {
                _logger.LogError(err.Message + Environment.NewLine + err.StackTrace);
                message = "Something went wrong, please try again a few minutes later";
                status = 500;
            }

            var responseDTO = new AuthResponseDTO
            {
                AuthVAResponse = new AuthVAResponse
                {
                    ResponseCode = Convert.ToString(status),
                    ResponseMessage = message
                }
            };

            return StatusCode(status, responseDTO);
        }
    }

}
