﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBTS_id.DTOs;

namespace TestBTS_id.Interfaces
{
    public interface IChecklistProvider
    {
        Task<IEnumerable<GetChecklistDTO>> GetChecklist();
        Task<string> InsertChecklist(string name);
        Task<int> RemoveChecklist(int checkId);
        Task<IEnumerable<GetChecklistItemDTO>> GetChecklistItem(int checkId);
        Task<string> AddChecklistItem(int checkId, string name);
        Task<string> GetChecklistItembyId(int checkId, int checkItemId);
        Task<string> UpdateChecklistItembyId(int checkId, int checkItemId, string name);
        Task<string> RemoveChecklistItemById(int checkId, int checkItemId);
    }
}
