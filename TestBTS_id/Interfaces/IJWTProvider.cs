﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestBTS_id.DTOs;
using TestBTS_id.Model;

namespace TestBTS_id.Interfaces
{
    public interface IJWTProvider
    {
        TokenResponseDTO GenerateToken(string username);
        Task<bool> AuthenticateUser(LoginRequestDTO requestDTO);
        Task RegisterUser(RegisterRequestDTO requestDTO);
        Task<AuthorizeModel> GetOneUser(string username);

    }
}
