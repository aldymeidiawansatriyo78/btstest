﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TestBTS_id.DTOs;
using TestBTS_id.Interfaces;
using TestBTS_id.Model;

namespace TestBTS_id.Providers
{
    public class JWTProvider : IJWTProvider
    {
        private readonly string _secret;
        private readonly DateTime _expiryDate;
        private readonly string _issuer;
        private readonly IDbConnection _dbConnection;


        public JWTProvider(IConfiguration config, IDbConnection dbConnection)
        {
            var jwtConfig = config.GetSection("JWTConfig");
            double nextDays = double.Parse(jwtConfig.GetSection("expirationInDays").Value);

            _secret = jwtConfig.GetSection("Secret").Value;
            _expiryDate = DateTime.Now.AddDays(nextDays);
            _issuer = jwtConfig.GetSection("issuer").Value;
            _dbConnection = dbConnection;
        }

        public TokenResponseDTO GenerateToken(string username)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _issuer,
                Subject = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, username) }),
                Expires = _expiryDate,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new TokenResponseDTO
            {
                Token = tokenHandler.WriteToken(token),
                ExpiredAt = _expiryDate
            };
        }

        public async Task<AuthorizeModel> GetOneUser(string username)
        {
            return await _dbConnection.QueryFirstOrDefaultAsync<AuthorizeModel>(
                sql: @"
                    SELECT TOP 1 USERNAME AS Username, PASSWORD AS password
                    FROM [DBAPItes].[dbo].[Authorize_Table]
                    WHERE USERNAME = @username",
                param: new { Username = username },
                commandType: CommandType.Text
            );
        }

        public async Task<bool> AuthenticateUser(LoginRequestDTO requestDTO)
        {
            AuthorizeModel user = await GetOneUser(requestDTO.Username);

            bool isUserValid = !(user == null || !BCrypt.Net.BCrypt.Verify(requestDTO.Password + _secret, user.password));

            return isUserValid;
        }

        public async Task RegisterUser(RegisterRequestDTO requestDTO)
        {
            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(requestDTO.Password + _secret);
            await _dbConnection.ExecuteAsync(
                sql: @"
                    INSERT [DBAPItes].[dbo].[Authorize_Table] (USERNAME, PASSWORD)
                    VALUES (@username, @password)    
                    ",
                param: new { requestDTO.Username, Password = hashedPassword },
                commandType: CommandType.Text
            );

        }
    }

}
