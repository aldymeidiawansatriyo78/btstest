﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using TestBTS_id.DTOs;
using Dapper;
using TestBTS_id.Interfaces;

namespace TestBTS_id.Providers
{
    public class ChecklistProvider : IChecklistProvider
    {
        private readonly IDbConnection _dbConnection;
        public ChecklistProvider(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        public async Task<IEnumerable<GetChecklistDTO>> GetChecklist()
        {
            var query = @"SELECT  [checklistId]
                              ,[name]
                              ,[type]
                              ,[checklistItemId]
                          FROM [DBAPItes].[dbo].[checklist_table]";
            return await _dbConnection.QueryAsync<GetChecklistDTO>(sql: query, commandType: CommandType.Text);
        }

        public async Task<string> InsertChecklist(string name)
        {

            var query = @"INSERT INTO [DBAPItes].[dbo].[checklist_table]
                                       ([name]
                                       ,[type]
                                       ,[checklistItemId])
                                 VALUES
                                       (@name1
                                       ,'Internal'
                                       ,'2')
	                             Select SCOPE_IDENTITY()";

            var name1 = name;
            string result = await _dbConnection.QueryFirstOrDefaultAsync<string>(sql: query, param: new { name1 }, commandType: CommandType.Text);

            return result;

        }

        public async Task<int> RemoveChecklist(int checkId)
        {

            var query = @" DELETE FROM [DBAPItes].[dbo].[checklist_table]
	                        WHERE checklistId = @checkid1
	                        Select @checkid1 ";

            var checkid1 = checkId;
            int result = await _dbConnection.QueryFirstOrDefaultAsync<int>(sql: query, param: new { checkid1 }, commandType: CommandType.Text);

            return result;

        }

        public async Task<IEnumerable<GetChecklistItemDTO>> GetChecklistItem(int checkId)
        {

            var query = @" Select [item_name]
                              From [DBAPItes].[dbo].[checklistItem_table] A
                              JOIN [DBAPItes].[dbo].[checklist_table] B ON B.checklistId = A.checklistId
                              WHERE B.checklistId = @checkid1 ";

            var checkid1 = checkId;
            return await _dbConnection.QueryAsync<GetChecklistItemDTO>(sql: query, param: new { checkid1 }, commandType: CommandType.Text);      

        }

        public async Task<string> AddChecklistItem(int checkId, string name)
        {

            var query = @"INSERT INTO [dbo].[checklistItem_table]
                                   ([item_name]
                                   ,[checklistId])
                             VALUES
                                   (@name1
                                   ,@checkid1)
                              SELECT @name1";

            var checkid1 = checkId;
            var name1 = name;
            return await _dbConnection.QueryFirstOrDefaultAsync<string>(sql: query, param: new { checkid1, name1 }, commandType: CommandType.Text);

        }

        public async Task<string> GetChecklistItembyId(int checkId, int checkItemId)
        {

            var query = @" Select [item_name]
                              From [DBAPItes].[dbo].[checklistItem_table] A
                              JOIN [DBAPItes].[dbo].[checklist_table] B ON B.checklistId = A.checklistId
                              WHERE B.checklistId = @checkid1 AND A.checklistItemId = @checkiditem1 ";

            var checkid1 = checkId;
            var checkiditem1 = checkItemId;
            return await _dbConnection.QueryFirstOrDefaultAsync<string>(sql: query, param: new { checkid1, checkiditem1 }, commandType: CommandType.Text);

        }

        public async Task<string> UpdateChecklistItembyId(int checkId, int checkItemId, string name)
        {

            var query = @" Update [DBAPItes].[dbo].[checklistItem_table]
                              SET [item_name] = @name1
                              WHERE checklistId = @checkid1 AND checklistItemId = @checkiditem1 
                                SELECT @name1 ";
                            
            var name1 = name;
            var checkid1 = checkId;
            var checkiditem1 = checkItemId;
            return await _dbConnection.QueryFirstOrDefaultAsync<string>(sql: query, param: new {name1, checkid1, checkiditem1 }, commandType: CommandType.Text);

        }

        public async Task<string> RemoveChecklistItemById(int checkId, int checkItemId)
        {

            var query = @" DELETE FROM [DBAPItes].[dbo].[checklistItem_table]
	                        WHERE checklistId = @checkid1 AND checklistItemId = @checkiditem1
	                          SELECT convert(varchar(100),@checkid1) + ' dengan item id = ' + convert(varchar(100),@checkiditem1 )";

            var checkid1 = checkId;
            var checkiditem1 = checkItemId;
            string result = await _dbConnection.QueryFirstOrDefaultAsync<string>(sql: query, param: new { checkid1, checkiditem1 }, commandType: CommandType.Text);

            return result;

        }


    }
}
