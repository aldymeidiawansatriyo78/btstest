﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestBTS_id.DTOs
{
    public class TokenResponseDTO
    {
        public string Token { get; set; }
        public DateTime ExpiredAt { get; set; }
    }
}
