﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestBTS_id.DTOs
{
    public class ResponseDTO
    {
        public string hasil { get; set; }

        public int status { get; set; }

        public string message { get; set; }
    }
}
