﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace TestBTS_id.DTOs
{
    public class GetChecklistDTO
    {
        public string name { get; set; }

        public string type { get; set; }
    }
}
