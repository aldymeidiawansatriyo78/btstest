﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestBTS_id.DTOs
{
    public class AuthResponseDTO
    {
        [JsonProperty("authVAResponse")]
        public AuthVAResponse AuthVAResponse { get; set; }
    }

    public class AuthVAResponse
    {
        [JsonProperty("responseCode")]
        public string ResponseCode { get; set; }

        [JsonProperty("responseMessage")]
        public string ResponseMessage { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TokenResponseDTO DetailResponse { get; set; }
    }

}
