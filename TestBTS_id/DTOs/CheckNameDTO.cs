﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestBTS_id.DTOs
{
    public class CheckNameDTO
    {
        public string name { get; set; }

        public string itemName { get; set; }
    }
}
