USE [DBAPItes]
GO
/****** Object:  Table [dbo].[checklistItem_table]    Script Date: 18/03/2023 19:31:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[checklistItem_table](
	[checklistItemId] [bigint] IDENTITY(1,1) NOT NULL,
	[item_name] [varchar](100) NULL,
	[checklistId] [bigint] NULL,
 CONSTRAINT [PK_checklistItem_table] PRIMARY KEY CLUSTERED 
(
	[checklistItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
